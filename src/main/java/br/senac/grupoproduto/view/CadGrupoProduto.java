package br.senac.grupoproduto.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.ButtonGroup;
import br.senac.produto.model.GrupoProduto;
import br.senac.grupoproduto.model.GrupoProdutoDAO;
import br.senac.produto.model.TipoProduto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class CadGrupoProduto extends javax.swing.JDialog {

   public static void main (String[] args){
        CadGrupoProduto cadGrupoProduto = new CadGrupoProduto(5);
        cadGrupoProduto.setVisible(true);
        
    }
    private JPanel pnBotoes;
    private JPanel pnTipoProduto;
    private Object java;
    private JPanel pnRadioTipo;
    private ButtonGroup rdGrupo;
    private GrupoProdutoDAO gpDAO = new GrupoProdutoDAO();
    private Integer idGrupoProduto;
    private JTextField txCodigo;
    private GrupoProduto grupoProduto;
    private JTextField txNome;
    private JRadioButton rdServico;
    private JRadioButton rdMercadoria;
    private JRadioButton rdMateriaPrima;  

    public CadGrupoProduto(Integer idGrupoProduto){
        configurarTela();
        configurarPanelTipoProduto();
        configurarPanelBotoes();
        configurarPanelDados();
        txNome.requestFocus();
        txCodigo.setEditable(false);
        this.idGrupoProduto = idGrupoProduto;
        if (idGrupoProduto==null){
            grupoProduto = new GrupoProduto();
            return;
        }
        grupoProduto = gpDAO.getPorId(idGrupoProduto);
        txCodigo.setText(grupoProduto.getIdGrupoProduto().toString());
        txNome.setText(grupoProduto.getNomeGrupoProduto());
        if(grupoProduto.getTipoProduto()==TipoProduto.SERVICO){
            rdServico.setSelected(true);
        } else if (grupoProduto.getTipoProduto()==TipoProduto.MATERIA_PRIMA){
            rdMateriaPrima.setSelected(true);
        } else {
            rdMercadoria.setSelected(true);
        }
    }

    private void configurarTela(){
        setTitle("Cadastro Grupo Produto");
        setSize(650,180);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setModal(true);
    }
    private void configurarPanelTipoProduto(){
        pnTipoProduto = new JPanel();
        pnTipoProduto.setLayout(new java.awt.GridLayout(3,2));
        JLabel lbCodigo = new JLabel("Código:");
        lbCodigo.setHorizontalAlignment(SwingConstants.LEFT);
        JLabel lbNome = new JLabel("Nome:");
        lbNome.setHorizontalAlignment(SwingConstants.LEFT);
        JLabel lbTipo = new JLabel("Tipo:");
        lbTipo.setHorizontalAlignment(SwingConstants.LEFT);
        txCodigo = new JTextField();
        txCodigo.setVisible(true);
        txCodigo.setHorizontalAlignment(SwingConstants.LEFT);
        txNome = new JTextField();
        txNome.setHorizontalAlignment(SwingConstants.LEFT);
        pnTipoProduto.add(lbCodigo);
        pnTipoProduto.add(txCodigo);
        pnTipoProduto.add(lbNome);
        pnTipoProduto.add(txNome);
        pnTipoProduto.add(lbTipo);
        add(pnTipoProduto);
    }
    private void configurarPanelBotoes(){
        pnBotoes = new JPanel();
        pnBotoes.setLayout(new java.awt.FlowLayout(FlowLayout.RIGHT));
        JButton btGravar = new JButton("Gravar");
        btGravar.setSize(new Dimension(80,30));
        JButton btCancelar = new JButton("Cancelar");
        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btCancelar.setSize(new Dimension(80,30));
        btGravar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gravar();
            }
        });
        pnBotoes.add(btGravar);
        pnBotoes.add(btCancelar);
        add(pnBotoes,BorderLayout.PAGE_END);        
    }
    public void gravar(){
        if(txNome.getText().trim().equals("")){
           JOptionPane.showMessageDialog(this, "Nome não informado!", "Erro", JOptionPane.ERROR_MESSAGE);
           txNome.requestFocus();
           return;
        }
        if(rdMercadoria.isSelected()==false && rdServico.isSelected()==false && rdMateriaPrima.isSelected()==false){
              JOptionPane.showMessageDialog(this, "Tipo não informado!", "Erro", JOptionPane.ERROR_MESSAGE);           
         }
        grupoProduto.setNomeGrupoProduto(txNome.getText());
        if (rdServico.isSelected()){
            grupoProduto.setTipoProduto(TipoProduto.SERVICO);
        } 
        if (rdMercadoria.isSelected()){
           grupoProduto.setTipoProduto(TipoProduto.MERCADORIA);
        }
        if (rdMateriaPrima.isSelected()){
           grupoProduto.setTipoProduto(TipoProduto.MATERIA_PRIMA);
        }
        if (grupoProduto.getIdGrupoProduto()==null){
            Integer idNovo = gpDAO.inserir(grupoProduto);
            JOptionPane.showMessageDialog(this, "Inserido com Sucesso!", "Inserir", JOptionPane.INFORMATION_MESSAGE); 

        } else {
           gpDAO.alterar(grupoProduto);
           JOptionPane.showMessageDialog(this, "Alterado com Sucesso!", "Alterar", JOptionPane.INFORMATION_MESSAGE); 
           
        }
        dispose();
    }
    private void configurarPanelDados(){        
        rdServico = new JRadioButton("Serviço");
        rdMercadoria = new JRadioButton("Mercadoria");
        rdMateriaPrima = new JRadioButton("Matéria Prima");  
        rdGrupo = new ButtonGroup();
        rdGrupo.add(rdServico);
        rdGrupo.add(rdMercadoria);
        rdGrupo.add(rdMateriaPrima);
        pnRadioTipo = new JPanel();
        pnRadioTipo.setLayout(new java.awt.FlowLayout(FlowLayout.LEFT));
        pnRadioTipo.add(rdServico);
        pnRadioTipo.add(rdMercadoria);
        pnRadioTipo.add(rdMateriaPrima);
        pnTipoProduto.add(pnRadioTipo);
    }      
}
