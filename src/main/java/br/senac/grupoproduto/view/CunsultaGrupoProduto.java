package br.senac.grupoproduto.view;

import br.senac.grupoproduto.model.GrupoProdutoDAO;
import br.senac.produto.model.GrupoProduto;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class CunsultaGrupoProduto extends javax.swing.JFrame {
    public CunsultaGrupoProduto() {
        initComponents();
        buscar();
    }
    public void buscar(){
        DefaultTableModel model = (DefaultTableModel) tbDados.getModel();
        model.setRowCount(0);
        GrupoProdutoDAO dao = new GrupoProdutoDAO();
        for (GrupoProduto gp : dao.listarPorNome(txNome.getText())){
            Object[] linha = new Object[3];
            linha[0] = gp.getIdGrupoProduto();
            linha[1] = gp.getNomeGrupoProduto();
            linha[2] = gp.getTipoProduto().getNome();
            model.addRow(linha);
        }
    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CunsultaGrupoProduto().setVisible(true);
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jToolBar2 = new javax.swing.JToolBar();
        btNovo = new javax.swing.JButton();
        btAlterar = new javax.swing.JButton();
        btExcluir = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btFechar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        pnBusca = new javax.swing.JPanel();
        lbNome = new javax.swing.JLabel();
        txNome = new javax.swing.JTextField();
        btBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbDados = new javax.swing.JTable();

        jToolBar1.setRollover(true);

        jLabel1.setText("Nome:");
        jToolBar1.add(jLabel1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        jPanel2.setLayout(new java.awt.BorderLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jToolBar2.setRollover(true);

        btNovo.setText("Novo");
        btNovo.setFocusable(false);
        btNovo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btNovo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNovoActionPerformed(evt);
            }
        });
        jToolBar2.add(btNovo);

        btAlterar.setText("Alterar");
        btAlterar.setFocusable(false);
        btAlterar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btAlterar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAlterarActionPerformed(evt);
            }
        });
        jToolBar2.add(btAlterar);

        btExcluir.setText("Excluir");
        btExcluir.setFocusable(false);
        btExcluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btExcluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btExcluirActionPerformed(evt);
            }
        });
        jToolBar2.add(btExcluir);
        jToolBar2.add(filler1);

        btFechar.setText("Fechar");
        btFechar.setFocusable(false);
        btFechar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btFechar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFecharActionPerformed(evt);
            }
        });
        jToolBar2.add(btFechar);

        getContentPane().add(jToolBar2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setLayout(new java.awt.BorderLayout());

        lbNome.setText("Nome:");

        btBuscar.setText("Buscar");
        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnBuscaLayout = new javax.swing.GroupLayout(pnBusca);
        pnBusca.setLayout(pnBuscaLayout);
        pnBuscaLayout.setHorizontalGroup(
            pnBuscaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnBuscaLayout.createSequentialGroup()
                .addComponent(lbNome)
                .addGap(4, 4, 4)
                .addComponent(txNome, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btBuscar)
                .addGap(0, 314, Short.MAX_VALUE))
        );
        pnBuscaLayout.setVerticalGroup(
            pnBuscaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnBuscaLayout.createSequentialGroup()
                .addGroup(pnBuscaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btBuscar)
                    .addComponent(lbNome))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.add(pnBusca, java.awt.BorderLayout.PAGE_START);

        tbDados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Descricão", "Tipo do Produto"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbDados);

        jPanel3.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarActionPerformed
        buscar();
    }//GEN-LAST:event_btBuscarActionPerformed

    private void btNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNovoActionPerformed
            CadGrupoProduto cadGP = new CadGrupoProduto(null);
            cadGP.setVisible(true);
            buscar();
    }//GEN-LAST:event_btNovoActionPerformed

    private void btFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btFecharActionPerformed

    private void btAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAlterarActionPerformed
     Integer indexSel = tbDados.getSelectedRow();
     if (indexSel==-1){
         JOptionPane.showMessageDialog(this, "Selecione um registro!","Aviso",JOptionPane.WARNING_MESSAGE);
     }
     Integer idGrupoProduto = (Integer)tbDados.getModel().getValueAt(indexSel, 0);
     CadGrupoProduto cadGrupoProd = new CadGrupoProduto(idGrupoProduto);
     cadGrupoProd.setVisible(true);
     buscar();
    }//GEN-LAST:event_btAlterarActionPerformed

    private void btExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btExcluirActionPerformed
     Integer indexSel = tbDados.getSelectedRow();
     if (indexSel==-1){
         JOptionPane.showMessageDialog(this, "Selecione um registro!","Aviso",JOptionPane.WARNING_MESSAGE);
     }
     Integer idGrupoProduto = (Integer)tbDados.getModel().getValueAt(indexSel, 0);
     int pergunta =  JOptionPane.showConfirmDialog(this, "Confirma Exclusão?","Exclusão",JOptionPane.YES_OPTION);
     if (pergunta==JOptionPane.YES_OPTION){
     GrupoProdutoDAO gpDAO = new GrupoProdutoDAO();
     gpDAO.excluir(idGrupoProduto);
     }
     buscar();
    }//GEN-LAST:event_btExcluirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAlterar;
    private javax.swing.JButton btBuscar;
    private javax.swing.JButton btExcluir;
    private javax.swing.JButton btFechar;
    private javax.swing.JButton btNovo;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JLabel lbNome;
    private javax.swing.JPanel pnBusca;
    private javax.swing.JTable tbDados;
    private javax.swing.JTextField txNome;
    // End of variables declaration//GEN-END:variables
}
