package br.senac.grupoproduto.model;
import br.senac.produto.model.GrupoProduto;
import br.senac.produto.model.TipoProduto;
import br.senac.grupoproduto.model.GrupoProdutoDAO;
import java.util.List;
public class GrupoProdutoDaoTeste {
    public static void main(String[] args){
        GrupoProdutoDAO dao = new GrupoProdutoDAO();
        GrupoProduto gpNovo = new GrupoProduto();
        gpNovo.setNomeGrupoProduto("Roupas");
        gpNovo.setTipoProduto(TipoProduto.MERCADORIA);
        Integer idNovoGP = dao.inserir(gpNovo);
        System.out.println("Novo id: "+idNovoGP);
        List<GrupoProduto> lista = dao.listarPorNome("a");
        System.out.println(lista);
        for (GrupoProduto gp: lista){
            System.out.println(gp.getNomeGrupoProduto());
        }
    }    
}
