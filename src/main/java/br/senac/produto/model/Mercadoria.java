package br.senac.produto.model;
public class Mercadoria extends Produto {
    private Long idMercadoria;
    private byte[] imagem;
    
    /*public Float getTotalPercImposto(){
        return ;
    }*/

    public Mercadoria(Long idMercadoria, byte[] imagem) {
        this.idMercadoria = idMercadoria;
        this.imagem = imagem;
    }

    public Mercadoria(Long idMercadoria, byte[] imagem, Long idProduto) {
        super(idProduto);
        this.idMercadoria = idMercadoria;
        this.imagem = imagem;
    }

    public Long getIdMercadoria() {
        return idMercadoria;
    }

    public void setIdMercadoria(Long idMercadoria) {
        this.idMercadoria = idMercadoria;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    @Override
    public Float getTotalPercImposto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
