package br.senac.produto.model;

import java.util.Objects;

public class GrupoProduto {

    private Integer idGrupoProduto;
    private String nomeGrupoProduto;
    private TipoProduto tipoProduto;

    public GrupoProduto(Integer idGrupoProduto, String nomeGrupoProduto, TipoProduto tipoProduto) {
        this.idGrupoProduto = idGrupoProduto;
        this.nomeGrupoProduto = nomeGrupoProduto;
        this.tipoProduto = tipoProduto;
    }

    public GrupoProduto() {

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.idGrupoProduto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupoProduto other = (GrupoProduto) obj;
        if (!Objects.equals(this.idGrupoProduto, other.idGrupoProduto)) {
            return false;
        }
        return true;
    }

    public Integer getIdGrupoProduto() {
        return idGrupoProduto;
    }

    public void setIdGrupoProduto(Integer idGrupoProduto) {
        this.idGrupoProduto = idGrupoProduto;
    }

    public String getNomeGrupoProduto() {
        return nomeGrupoProduto;
    }

    public void setNomeGrupoProduto(String nomeGrupoProduto) {
        this.nomeGrupoProduto = nomeGrupoProduto;
    }

    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    @Override
    public String toString() {
        return "GrupoProduto{" + "idGrupoProduto=" + idGrupoProduto + ", nomeGrupoProduto=" + nomeGrupoProduto + ", tipoProduto=" + tipoProduto + '}';
    }

}
