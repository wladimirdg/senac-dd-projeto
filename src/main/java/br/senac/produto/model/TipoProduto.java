package br.senac.produto.model;
public enum TipoProduto {
	MERCADORIA(1, "Mercadoria"), SERVICO(2, "Serviço"), MATERIA_PRIMA(3, "Matéria Prima"); 
	private Integer id; 
        private String nome;
	private TipoProduto(Integer id, String nome){
		this.id = id;
                this.nome = nome;
	}
   	public Integer getId() {
		return id;
	}
        public String getNome(){
            return nome;
        }
}